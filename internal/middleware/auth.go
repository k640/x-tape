package middleware

import (
	"crypto/rsa"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

func AuthenticateUserMiddleware(enviroment string, publicKey *rsa.PublicKey) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if enviroment == "development1" {
			ctx.Set("user_id", uint64(1))
			ctx.Next()
			return
		}
		tokenString := ctx.GetHeader("Authorization")
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			// Don't forget to validate the alg is what you expect:
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			return publicKey, nil
		})
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "unauthorized"})
			return
		}
		claims, ok := token.Claims.(jwt.MapClaims)
		if !(ok && token.Valid) {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "unauthorized"})
			return

		}
		userId := uint64(claims["id"].(float64))

		ctx.Set("user_id", userId)
		fmt.Println(userId)
		ctx.Next()
	}
}
