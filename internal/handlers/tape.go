package handlers

import (
	"context"
	"crypto/rand"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/k640/tape/internal/core"
)

type TapeService interface {
	Create(ctx context.Context, tape *core.Tape) error
	GetAllByUserId(ctx context.Context, id uint64) ([]*core.Tape, error)
	GetById(ctx context.Context, id uint64) (*core.Tape, error)
	GetRandom(ctx context.Context, user_id uint64, mask uint16) (*core.Tape, error)
}

type TapeHandler struct {
	service TapeService
}

//create a new tape gin http Handler with a tapeService
func NewTapeHandler(service TapeService) *TapeHandler {
	return &TapeHandler{
		service: service,
	}
}

func (t *TapeHandler) Create(c *gin.Context) {
	var tape core.Tape
	if err := c.Bind(&tape); err != nil {

		c.JSON(http.StatusOK, gin.H{
			"message": err.Error(),
		})
		return
	}
	if tape.Video.Header.Get("Content-Type") != "video/mp4" {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid video format",
		})
		return
	}
	str, err := random_filename_16_char()
	filename := str + ".mp4"
	err = c.SaveUploadedFile(tape.Video, "uploads/videos/"+filename)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	tape.Path = filename
	userId := c.Value("user_id")

	tape.UserId = userId.(uint64)

	err = t.service.Create(c, &tape)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": filename,
	})
}

func random_filename_16_char() (s string, err error) {
	b := make([]byte, 8)
	_, err = rand.Read(b)
	if err != nil {
		return
	}
	s = fmt.Sprintf("%x", b)
	return
}

func (t *TapeHandler) GetRandom(c *gin.Context) {
	userId := c.Value("user_id")
	mask := c.Query("mask")
	if mask == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "mask is required",
		})
		return
	}
	maskInt, err := strconv.ParseUint(mask, 16, 16)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "invalid mask",
		})
		return
	}
	tape, err := t.service.GetRandom(c, userId.(uint64), uint16(maskInt))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, tape)
}

func (t *TapeHandler) GetAllByUserId(c *gin.Context) {
	// userId :c.GetQuery("user_id")
	var userId uint64
	if queryParam, ok := c.GetQuery("user_id"); ok {
		number, err := strconv.ParseUint(queryParam, 10, 64)
		if err != nil {
			c.Header("Content-Type", "application/json")
			c.JSON(http.StatusNotFound,
				gin.H{"Error: ": "Invalid user_id on search filter!"})
			c.Abort()
			return
		}
		userId = number

	} else {
		userId = c.Value("user_id").(uint64)
	}
	tapes, err := t.service.GetAllByUserId(c, userId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, tapes)
}
