package repository

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/k640/tape/internal/core"
)

type TapeRepository struct {
	db *sqlx.DB
}

// creates a new tape repository
func NewTapeRepository(db *sqlx.DB) *TapeRepository {
	return &TapeRepository{
		db: db,
	}
}

// creates a new tape
func (r *TapeRepository) Create(ctx context.Context, tape *core.Tape) error {
	_, err := r.db.NamedExec(`
		INSERT INTO tapes (
			description,
			user_id,
			mask,
			path
		) VALUES (
			:description,
			:user_id,
			:mask,
			:path
		)`, tape)
	return err
}

func (r *TapeRepository) GetRandom(ctx context.Context, user_id uint64, mask uint16) (*core.Tape, error) {
	tape := &core.Tape{}
	err := r.db.Get(tape, `
		SELECT * FROM tapes 
		WHERE user_id != ? AND ((mask & ?) > 0)
		ORDER BY RAND() LIMIT 1	
	`, user_id, mask)
	return tape, err
}

// gets a tape by id
func (r *TapeRepository) GetById(ctx context.Context, id uint64) (*core.Tape, error) {
	tape := &core.Tape{}
	err := r.db.Get(tape, "SELECT * FROM tape WHERE id = ?", id)
	return tape, err
}

// selects all users tapes from database
func (r *TapeRepository) GetAllByUserId(ctx context.Context, id uint64) ([]*core.Tape, error) {
	tapes := []*core.Tape{}
	err := r.db.Select(&tapes, "SELECT * FROM tapes WHERE user_id = ?", id)
	return tapes, err
}
