package services

import (
	"context"

	"gitlab.com/k640/tape/internal/core"
)

type TapeRepository interface {
	Create(ctx context.Context, tape *core.Tape) error
	GetAllByUserId(ctx context.Context, id uint64) ([]*core.Tape, error)
	GetById(ctx context.Context, id uint64) (*core.Tape, error)
	GetRandom(ctx context.Context, user_id uint64, mask uint16) (*core.Tape, error)
}

type TapeService struct {
	repository TapeRepository
}

func NewTapeService(repository TapeRepository) *TapeService {
	return &TapeService{
		repository: repository,
	}
}

func (t *TapeService) Create(ctx context.Context, tape *core.Tape) error {
	return t.repository.Create(ctx, tape)
}

func (t *TapeService) GetAllByUserId(ctx context.Context, id uint64) ([]*core.Tape, error) {
	return t.repository.GetAllByUserId(ctx, id)
}

func (t *TapeService) GetById(ctx context.Context, id uint64) (*core.Tape, error) {
	return t.repository.GetById(ctx, id)
}

func (t *TapeService) GetRandom(ctx context.Context, user_id uint64, mask uint16) (*core.Tape, error) {
	return t.repository.GetRandom(ctx, user_id, mask)
}
