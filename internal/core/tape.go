package core

import "mime/multipart"

type Tape struct {
	Id          uint64                `json:"id"`
	Description string                `form:"description" binding:"required" json:"description" db:"description"`
	Mask        uint16                `form:"mask" binding:"required" json:"mask" db:"mask"`
	Video       *multipart.FileHeader `form:"video" binding:"required"`
	Path        string                `json:"path" db:"path"`
	UserId      uint64                `json:"user_id" db:"user_id"`
	CreatedAt   string                `json:"created_at" db:"created_at"`
}
