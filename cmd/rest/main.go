package main

import (
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/k640/tape/internal/handlers"
	"gitlab.com/k640/tape/internal/middleware"
	"gitlab.com/k640/tape/internal/repository"
	"gitlab.com/k640/tape/internal/services"
	"gitlab.com/k640/tape/pkg/client/mysql"
)

func getPublicKey() *rsa.PublicKey {

	verifyBytes, err := ioutil.ReadFile("./certs/public.pem")

	if err != nil {
		panic(err)
	}

	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		panic(err)
	}
	return publicKey
}

// gets eviroment state from enviroment variable or default value "development"
func getEviroment() string {

	enviroment := os.Getenv("ENVIROMENT")
	if enviroment == "" {
		enviroment = "development"
	}
	return enviroment
}

func main() {
	r := gin.Default()
	db := mysql.GetClient("tape", "tape", "db:3306", "tape")
	// db := mysql.GetClient("tape", "tape", "127.0.0.1:5555", "tape")
	publicKey := getPublicKey()
	enviroment := getEviroment()
	r.Use(func(ctx *gin.Context) {
		ctx.Set("enviroment", enviroment)
		ctx.Set("publicKey", publicKey)
		fmt.Println("seting enviroment to: ", enviroment)
		ctx.Next()
	})
	tapeGroup := r.Group("/")

	tapeGroup.Use(middleware.AuthenticateUserMiddleware(enviroment, publicKey))
	tapeRepo := repository.NewTapeRepository(db)
	tapeService := services.NewTapeService(tapeRepo)
	tapeHandler := handlers.NewTapeHandler(tapeService)
	tapeGroup.POST("/create", tapeHandler.Create)
	tapeGroup.GET("/get", tapeHandler.GetRandom)
	tapeGroup.GET("/user", tapeHandler.GetAllByUserId)

	r.Run(":80")
	// r.Run(":5556")
}
