FROM mysql:5.7

ADD schema/tapes.sql /docker-entrypoint-initdb.d

EXPOSE 3306