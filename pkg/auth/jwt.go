package auth

import (
	"crypto/rsa"
)

type JWT struct {
	publicKey *rsa.PublicKey
}

func NewJWT(publicKey *rsa.PublicKey) *JWT {
	return &JWT{
		publicKey: publicKey,
	}
}

// type Claims interface {
// 	jwt.Claims,
// 	Id int64,
// }

// func (j *JWT) Parse(token string) (int64, error) {
// 	token, _ := jwt.ParseWithClaims(tokenString, func(t *jwt.Token) (interface{}, error) {
// 		return public, nil
// 	})
// 	return token
// }
