package mysql

import (
	"fmt"
	"time"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

func GetClient(user string, password string, address string, dbname string) *sqlx.DB {
	cfg := mysql.Config{
		User:                 user,
		Passwd:               password,
		Net:                  "tcp",
		Addr:                 address,
		DBName:               dbname,
		AllowNativePasswords: true,
	}
	fmt.Println(cfg.FormatDSN())
	db, err := sqlx.Open("mysql", cfg.FormatDSN())
	if err != nil {
		panic(err)
	}
	db.SetConnMaxLifetime(time.Second * 10)
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	return db

}
