FROM golang:1.18-buster AS build

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . /app
RUN ls -la /

RUN go build -o /build ./cmd/rest/


##
## Deploy
##
FROM ubuntu

WORKDIR /

COPY --from=build /build /build
RUN mkdir -p /uploads/videos

EXPOSE 80


ENTRYPOINT ["/build"]